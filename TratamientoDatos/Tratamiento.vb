﻿Imports Clases
Public Class Tratamiento
    Dim cadCon As String = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source =BaseDatosComidas.accdb"
    Dim conBD As New OleDb.OleDbConnection(cadCon)


    'Devuelve todas las comidas
    Public Function CargarComidas() As List(Of Comida)
        Dim comidas As New List(Of Comida)
        conBD.Open()
        Dim sql As String = "SELECT * FROM Comidas"
        Dim cmdCargarComidas As New OleDb.OleDbCommand(sql, conBD)
        Dim dr As OleDb.OleDbDataReader = cmdCargarComidas.ExecuteReader
        While dr.Read
                Dim comidaLeida As New Comida
                comidaLeida.ID = dr.Item("Id")
                comidaLeida.Nombre = dr.Item("Nombre")
                comidaLeida.Puntos = dr.Item("Puntos")
                comidaLeida.Tipo = dr.Item("Tipo")
                comidas.Add(comidaLeida)
        End While
        conBD.Close()
        Return comidas
    End Function

    'Devuelve todos los ingredientes
    Public Function CargarIngredientes() As List(Of Ingrediente)
        Dim ingredientes As New List(Of Ingrediente)
        Dim sql As String = "SELECT * FROM Ingredientes"
        conBD.Open()
        Dim cmdCargarIngredientes As New OleDb.OleDbCommand(sql, conBD)
        Dim dr As OleDb.OleDbDataReader = cmdCargarIngredientes.ExecuteReader
        While dr.Read
            Dim ingredienteLeido As New Ingrediente
            ingredienteLeido.ID = dr.Item("Id")
            ingredienteLeido.Nombre = dr.Item("Nombre")
            ingredienteLeido.Disponibilidad = dr.Item("Disponibilidad")
            ingredientes.Add(ingredienteLeido)
        End While
        conBD.Close()
        Return ingredientes
    End Function

    'Devuelve todos los ingredientes de una comida
    Public Function BuscarIngredientesDeUnaComida(ByVal nombreComida As String) As List(Of Ingrediente)
        Dim ingredientesDeComida As New List(Of Ingrediente)
        Dim sql As String = "SELECT * FROM Ingredientes INNER JOIN (Comidas INNER JOIN [Comidas/Ingredientes] ON Comidas.Id = [Comidas/Ingredientes].IdComida) ON Ingredientes.Id = [Comidas/Ingredientes].IdComida
WHERE (((Comidas.Nombre)=@NOMBRE));"
        conBD.Open()
        Dim cmdBuscarIngredientes As New OleDb.OleDbCommand(sql, conBD)
        cmdBuscarIngredientes.Parameters.AddWithValue("@NOMBRE", nombreComida)
        Dim dr As OleDb.OleDbDataReader = cmdBuscarIngredientes.ExecuteReader
        While dr.Read
            Dim ingredienteLeido As New Ingrediente
            ingredienteLeido.ID = dr.Item("Id")
            ingredienteLeido.Nombre = dr.Item("Nombre")
            ingredienteLeido.Disponibilidad = dr.Item("Disponibilidad")
            ingredientesDeComida.Add(ingredienteLeido)
        End While
        conBD.Close()
        Return ingredientesDeComida
    End Function

    'Devuelve las comidas que contienen un ingrediente buscado por el nombre del ingrediente
    Public Function BuscarComidasPorIngrediente(ByVal nombreIngrediente As String) As List(Of Comida)
        Dim comidasPorIngrediente As New List(Of Comida)
        conBD.Open()
        Dim sql As String = "SELECT * FROM Comidas INNER JOIN ([Comidas/Ingredientes] INNER JOIN  Ingredientes ON [Comidas/Ingredientes].IdComida = Comidas.Id) ON Ingredientes.Id = [Comidas/Ingredientes].IdIngrediente
WHERE (Ingredientes.Nombre = @NOMBRE);"
        Dim cmdBuscarComidasPorIngrediente As New OleDb.OleDbCommand(sql, conBD)
        cmdBuscarComidasPorIngrediente.Parameters.AddWithValue("@NOMBRE", nombreIngrediente)
        Dim dr As OleDb.OleDbDataReader = cmdBuscarComidasPorIngrediente.ExecuteReader
        While dr.Read
            Dim comidaLeida As New Comida
            comidaLeida.ID = dr.Item("Id")
            comidaLeida.Nombre = dr.Item("Nombre")
            comidaLeida.Puntos = dr.Item("Puntos")
            comidaLeida.Tipo = dr.Item("Tipo")
            comidasPorIngrediente.Add(comidaLeida)
        End While
        conBD.Close()
        Return comidasPorIngrediente
    End Function

    'Devuelve una comida random
    Public Function ComidaRandom() As Comida
        Dim comidaLeida As New Comida
        Dim sql As String = "SELECT MAX(Comidas.Id) FROM Comidas"
        Dim cmdMaxNumComidas As New OleDb.OleDbCommand(sql, conBD)
        conBD.Open()
        Dim maxComidas As Integer = cmdMaxNumComidas.ExecuteScalar
        Dim numeroRandom As Integer = CInt(Int((maxComidas * Rnd()) + 1))
        sql = "SELECT * FROM Comidas WHERE Comidas.Id = @RANDOM"
        Dim cmdComidaRandom As New OleDb.OleDbCommand(sql, conBD)
        cmdComidaRandom.Parameters.AddWithValue("@RANDOM", numeroRandom)
        Dim dr As OleDb.OleDbDataReader = cmdComidaRandom.ExecuteReader
        While dr.Read
            comidaLeida.ID = dr.Item("Id")
            comidaLeida.Nombre = dr.Item("Nombre")
            comidaLeida.Puntos = dr.Item("Puntos")
            comidaLeida.Tipo = dr.Item("Tipo")
        End While
        conBD.Close()
        Return comidaLeida
    End Function
End Class
