﻿Imports Clases
Public Class frmAñadirComidaOIngrediente
    Public todosLosIngredientes As New List(Of Ingrediente)

    Private Sub frmLoad(sender As Object, e As EventArgs) Handles MyBase.Load
        todosLosIngredientes = trat.CargarIngredientes
        For Each control As Control In Me.Controls
            control.Anchor = AnchorStyles.Bottom And AnchorStyles.Top And AnchorStyles.Right And AnchorStyles.Left
        Next

    End Sub

    Private Sub btnCambiarAComida_Click(sender As Object, e As EventArgs) Handles btnCambiarAComida.Click
        btnCambiarAIngrediente.Enabled = True
        btnCambiarAComida.Enabled = False
        lstDisponibles.Items.Clear()
        For Each ingrediente In todosLosIngredientes
            lstDisponibles.Items.Add(ingrediente.Nombre)
        Next
        txtNombre.Enabled = True
        lblNombre.Enabled = True
        lblNombre.Visible = True
        txtNombre.Visible = True
        txtTipo.Enabled = True
        lblTipo.Enabled = True
        lblIngredientesDisponibles.Enabled = True
        lblIngredientesSeleccionados.Enabled = True
        btnSeleccionarIngrediente.Enabled = True
        btnDesseleccionarIngrediente.Enabled = True
        btnListo.Enabled = True
        lstDisponibles.Enabled = True
        lstSeleccionados.Enabled = True
        txtTipo.Visible = True
        lblTipo.Visible = True
        lblIngredientesSeleccionados.Visible = True
        lblIngredientesDisponibles.Visible = True
        btnSeleccionarIngrediente.Visible = True
        btnDesseleccionarIngrediente.Visible = True
        btnListo.Visible = True
        lstSeleccionados.Visible = True
        lstDisponibles.Visible = True
    End Sub

    Private Sub btnCambiarAIngrediente_Click(sender As Object, e As EventArgs) Handles btnCambiarAIngrediente.Click
        txtNombre.Enabled = True
        lblNombre.Enabled = True
        lblNombre.Visible = True
        txtNombre.Visible = True
        txtTipo.Enabled = False
        lblTipo.Enabled = False
        lblIngredientesDisponibles.Enabled = False
        lblIngredientesSeleccionados.Enabled = False
        btnSeleccionarIngrediente.Enabled = False
        btnDesseleccionarIngrediente.Enabled = False
        btnListo.Enabled = True
        lstDisponibles.Enabled = False
        lstSeleccionados.Enabled = False
        txtTipo.Visible = False
        lblTipo.Visible = False
        lblIngredientesSeleccionados.Visible = False
        lblIngredientesDisponibles.Visible = False
        btnSeleccionarIngrediente.Visible = False
        btnDesseleccionarIngrediente.Visible = False
        btnListo.Visible = True
        lstSeleccionados.Visible = False
        lstDisponibles.Visible = False
        btnCambiarAComida.Enabled = True
        btnCambiarAIngrediente.Enabled = False
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs)
        frmInicio.Show()
        Me.Close()
    End Sub

    Private Sub btnSeleccionarIngrediente_Click(sender As Object, e As EventArgs) Handles btnSeleccionarIngrediente.Click
        lstSeleccionados.Items.Add(lstDisponibles.SelectedItem)
        lstDisponibles.Items.Remove(lstDisponibles.SelectedItem)
    End Sub

    Private Sub btnDesseleccionarIngrediente_Click(sender As Object, e As EventArgs) Handles btnDesseleccionarIngrediente.Click
        lstDisponibles.Items.Add(lstSeleccionados.SelectedItem)
        lstSeleccionados.Items.Remove(lstSeleccionados.SelectedItem)
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
    End Sub
End Class