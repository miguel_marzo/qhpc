﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSugerencias
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSugerencias))
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.btnSiguienteSugerencia = New System.Windows.Forms.Button()
        Me.pctboxComida = New System.Windows.Forms.PictureBox()
        Me.lblSugerenciaDescripcion = New System.Windows.Forms.Label()
        Me.lblSugerenciaNombre = New System.Windows.Forms.Label()
        Me.btnVerFoto = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        CType(Me.pctboxComida, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitulo
        '
        Me.lblTitulo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitulo.BackColor = System.Drawing.Color.Transparent
        Me.lblTitulo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblTitulo.Font = New System.Drawing.Font("Berlin Sans FB", 40.2!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.Peru
        Me.lblTitulo.Location = New System.Drawing.Point(12, 9)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(1258, 96)
        Me.lblTitulo.TabIndex = 1
        Me.lblTitulo.Text = "Sugerencias para comer"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSiguienteSugerencia
        '
        Me.btnSiguienteSugerencia.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSiguienteSugerencia.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.btnSiguienteSugerencia.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnSiguienteSugerencia.Font = New System.Drawing.Font("Berlin Sans FB Demi", 28.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSiguienteSugerencia.ForeColor = System.Drawing.Color.Peru
        Me.btnSiguienteSugerencia.Location = New System.Drawing.Point(160, 640)
        Me.btnSiguienteSugerencia.Name = "btnSiguienteSugerencia"
        Me.btnSiguienteSugerencia.Size = New System.Drawing.Size(719, 69)
        Me.btnSiguienteSugerencia.TabIndex = 2
        Me.btnSiguienteSugerencia.Text = "¡Siguiente sugerencia!"
        Me.btnSiguienteSugerencia.UseVisualStyleBackColor = False
        '
        'pctboxComida
        '
        Me.pctboxComida.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pctboxComida.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pctboxComida.Location = New System.Drawing.Point(963, 268)
        Me.pctboxComida.Name = "pctboxComida"
        Me.pctboxComida.Size = New System.Drawing.Size(307, 207)
        Me.pctboxComida.TabIndex = 3
        Me.pctboxComida.TabStop = False
        '
        'lblSugerenciaDescripcion
        '
        Me.lblSugerenciaDescripcion.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSugerenciaDescripcion.BackColor = System.Drawing.Color.LightSkyBlue
        Me.lblSugerenciaDescripcion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblSugerenciaDescripcion.Font = New System.Drawing.Font("Berlin Sans FB", 24.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSugerenciaDescripcion.ForeColor = System.Drawing.Color.Peru
        Me.lblSugerenciaDescripcion.Location = New System.Drawing.Point(108, 340)
        Me.lblSugerenciaDescripcion.Name = "lblSugerenciaDescripcion"
        Me.lblSugerenciaDescripcion.Size = New System.Drawing.Size(836, 261)
        Me.lblSugerenciaDescripcion.TabIndex = 4
        Me.lblSugerenciaDescripcion.Text = "*Descripcion Sugerencia*"
        '
        'lblSugerenciaNombre
        '
        Me.lblSugerenciaNombre.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSugerenciaNombre.BackColor = System.Drawing.Color.LightSkyBlue
        Me.lblSugerenciaNombre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblSugerenciaNombre.Font = New System.Drawing.Font("Berlin Sans FB", 28.2!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSugerenciaNombre.ForeColor = System.Drawing.Color.Peru
        Me.lblSugerenciaNombre.Location = New System.Drawing.Point(116, 186)
        Me.lblSugerenciaNombre.Name = "lblSugerenciaNombre"
        Me.lblSugerenciaNombre.Size = New System.Drawing.Size(670, 60)
        Me.lblSugerenciaNombre.TabIndex = 5
        Me.lblSugerenciaNombre.Text = "*Nombre Sugerencia*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lblSugerenciaNombre.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnVerFoto
        '
        Me.btnVerFoto.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVerFoto.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.btnVerFoto.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnVerFoto.Font = New System.Drawing.Font("Berlin Sans FB Demi", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVerFoto.ForeColor = System.Drawing.Color.Peru
        Me.btnVerFoto.Location = New System.Drawing.Point(990, 481)
        Me.btnVerFoto.Name = "btnVerFoto"
        Me.btnVerFoto.Size = New System.Drawing.Size(257, 54)
        Me.btnVerFoto.TabIndex = 6
        Me.btnVerFoto.Text = "Ver foto"
        Me.btnVerFoto.UseVisualStyleBackColor = False
        '
        'btnVolver
        '
        Me.btnVolver.BackColor = System.Drawing.Color.Transparent
        Me.btnVolver.BackgroundImage = Global.Formularios.My.Resources.Resources.flecha_atras
        Me.btnVolver.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVolver.Location = New System.Drawing.Point(12, 9)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(119, 107)
        Me.btnVolver.TabIndex = 7
        Me.btnVolver.UseVisualStyleBackColor = False
        '
        'frmSugerencias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Formularios.My.Resources.Resources.chalkboard_or_blackboard
        Me.ClientSize = New System.Drawing.Size(1282, 753)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnVerFoto)
        Me.Controls.Add(Me.lblSugerenciaNombre)
        Me.Controls.Add(Me.lblSugerenciaDescripcion)
        Me.Controls.Add(Me.pctboxComida)
        Me.Controls.Add(Me.btnSiguienteSugerencia)
        Me.Controls.Add(Me.lblTitulo)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSugerencias"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "¿Que hay para comer?"
        Me.TopMost = True
        CType(Me.pctboxComida, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblTitulo As Label
    Friend WithEvents btnSiguienteSugerencia As Button
    Friend WithEvents pctboxComida As PictureBox
    Friend WithEvents lblSugerenciaDescripcion As Label
    Friend WithEvents lblSugerenciaNombre As Label
    Friend WithEvents btnVerFoto As Button
    Friend WithEvents btnVolver As Button
End Class
