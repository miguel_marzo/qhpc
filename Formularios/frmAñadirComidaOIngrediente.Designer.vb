﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAñadirComidaOIngrediente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAñadirComidaOIngrediente))
        Me.btnCambiarAComida = New System.Windows.Forms.Button()
        Me.btnCambiarAIngrediente = New System.Windows.Forms.Button()
        Me.lblQueAñadir = New System.Windows.Forms.Label()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtTipo = New System.Windows.Forms.TextBox()
        Me.lstDisponibles = New System.Windows.Forms.ListBox()
        Me.lblIngredientesDisponibles = New System.Windows.Forms.Label()
        Me.lstSeleccionados = New System.Windows.Forms.ListBox()
        Me.lblIngredientesSeleccionados = New System.Windows.Forms.Label()
        Me.btnListo = New System.Windows.Forms.Button()
        Me.btnSeleccionarIngrediente = New System.Windows.Forms.Button()
        Me.btnDesseleccionarIngrediente = New System.Windows.Forms.Button()
        Me.pctboxIngrediente = New System.Windows.Forms.PictureBox()
        Me.pctboxComida = New System.Windows.Forms.PictureBox()
        Me.btnVolver = New System.Windows.Forms.Button()
        CType(Me.pctboxIngrediente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctboxComida, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCambiarAComida
        '
        Me.btnCambiarAComida.Font = New System.Drawing.Font("Berlin Sans FB", 28.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCambiarAComida.ForeColor = System.Drawing.Color.Peru
        Me.btnCambiarAComida.Location = New System.Drawing.Point(12, 129)
        Me.btnCambiarAComida.Name = "btnCambiarAComida"
        Me.btnCambiarAComida.Size = New System.Drawing.Size(277, 120)
        Me.btnCambiarAComida.TabIndex = 0
        Me.btnCambiarAComida.Text = "Añadir Comida"
        Me.btnCambiarAComida.UseVisualStyleBackColor = True
        '
        'btnCambiarAIngrediente
        '
        Me.btnCambiarAIngrediente.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnCambiarAIngrediente.Font = New System.Drawing.Font("Berlin Sans FB", 28.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCambiarAIngrediente.ForeColor = System.Drawing.Color.Peru
        Me.btnCambiarAIngrediente.Location = New System.Drawing.Point(993, 138)
        Me.btnCambiarAIngrediente.Name = "btnCambiarAIngrediente"
        Me.btnCambiarAIngrediente.Size = New System.Drawing.Size(277, 120)
        Me.btnCambiarAIngrediente.TabIndex = 1
        Me.btnCambiarAIngrediente.Text = "Añadir Ingrediente"
        Me.btnCambiarAIngrediente.UseVisualStyleBackColor = True
        '
        'lblQueAñadir
        '
        Me.lblQueAñadir.BackColor = System.Drawing.Color.DimGray
        Me.lblQueAñadir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblQueAñadir.Font = New System.Drawing.Font("Berlin Sans FB", 25.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQueAñadir.ForeColor = System.Drawing.Color.Peru
        Me.lblQueAñadir.Location = New System.Drawing.Point(238, 8)
        Me.lblQueAñadir.Name = "lblQueAñadir"
        Me.lblQueAñadir.Size = New System.Drawing.Size(900, 107)
        Me.lblQueAñadir.TabIndex = 6
        Me.lblQueAñadir.Text = "¿Que quieres añadir?"
        Me.lblQueAñadir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNombre
        '
        Me.lblNombre.BackColor = System.Drawing.SystemColors.HotTrack
        Me.lblNombre.Enabled = False
        Me.lblNombre.Font = New System.Drawing.Font("Berlin Sans FB", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.Location = New System.Drawing.Point(339, 283)
        Me.lblNombre.MinimumSize = New System.Drawing.Size(150, 50)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(150, 50)
        Me.lblNombre.TabIndex = 0
        Me.lblNombre.Text = "Nombre:"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblNombre.Visible = False
        '
        'lblTipo
        '
        Me.lblTipo.BackColor = System.Drawing.SystemColors.HotTrack
        Me.lblTipo.Enabled = False
        Me.lblTipo.Font = New System.Drawing.Font("Berlin Sans FB", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipo.Location = New System.Drawing.Point(827, 436)
        Me.lblTipo.MinimumSize = New System.Drawing.Size(150, 50)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(150, 50)
        Me.lblTipo.TabIndex = 1
        Me.lblTipo.Text = "Tipo:"
        Me.lblTipo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblTipo.Visible = False
        '
        'txtNombre
        '
        Me.txtNombre.Enabled = False
        Me.txtNombre.Font = New System.Drawing.Font("Berlin Sans FB", 28.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(517, 283)
        Me.txtNombre.MinimumSize = New System.Drawing.Size(150, 50)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(438, 59)
        Me.txtNombre.TabIndex = 2
        Me.txtNombre.Visible = False
        '
        'txtTipo
        '
        Me.txtTipo.Enabled = False
        Me.txtTipo.Font = New System.Drawing.Font("Berlin Sans FB", 28.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTipo.Location = New System.Drawing.Point(1005, 436)
        Me.txtTipo.MinimumSize = New System.Drawing.Size(150, 50)
        Me.txtTipo.Name = "txtTipo"
        Me.txtTipo.Size = New System.Drawing.Size(203, 59)
        Me.txtTipo.TabIndex = 3
        Me.txtTipo.Visible = False
        '
        'lstDisponibles
        '
        Me.lstDisponibles.Enabled = False
        Me.lstDisponibles.Font = New System.Drawing.Font("Berlin Sans FB", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstDisponibles.FormattingEnabled = True
        Me.lstDisponibles.ItemHeight = 30
        Me.lstDisponibles.Location = New System.Drawing.Point(47, 416)
        Me.lstDisponibles.Name = "lstDisponibles"
        Me.lstDisponibles.Size = New System.Drawing.Size(210, 304)
        Me.lstDisponibles.TabIndex = 4
        Me.lstDisponibles.Visible = False
        '
        'lblIngredientesDisponibles
        '
        Me.lblIngredientesDisponibles.BackColor = System.Drawing.SystemColors.HotTrack
        Me.lblIngredientesDisponibles.Enabled = False
        Me.lblIngredientesDisponibles.Font = New System.Drawing.Font("Berlin Sans FB", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIngredientesDisponibles.Location = New System.Drawing.Point(12, 363)
        Me.lblIngredientesDisponibles.MinimumSize = New System.Drawing.Size(150, 50)
        Me.lblIngredientesDisponibles.Name = "lblIngredientesDisponibles"
        Me.lblIngredientesDisponibles.Size = New System.Drawing.Size(298, 50)
        Me.lblIngredientesDisponibles.TabIndex = 5
        Me.lblIngredientesDisponibles.Text = "Ingredientes disponibles"
        Me.lblIngredientesDisponibles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblIngredientesDisponibles.Visible = False
        '
        'lstSeleccionados
        '
        Me.lstSeleccionados.Enabled = False
        Me.lstSeleccionados.Font = New System.Drawing.Font("Berlin Sans FB", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstSeleccionados.FormattingEnabled = True
        Me.lstSeleccionados.ItemHeight = 30
        Me.lstSeleccionados.Location = New System.Drawing.Point(479, 416)
        Me.lstSeleccionados.Name = "lstSeleccionados"
        Me.lstSeleccionados.Size = New System.Drawing.Size(210, 304)
        Me.lstSeleccionados.TabIndex = 6
        Me.lstSeleccionados.Visible = False
        '
        'lblIngredientesSeleccionados
        '
        Me.lblIngredientesSeleccionados.BackColor = System.Drawing.SystemColors.HotTrack
        Me.lblIngredientesSeleccionados.Enabled = False
        Me.lblIngredientesSeleccionados.Font = New System.Drawing.Font("Berlin Sans FB", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIngredientesSeleccionados.Location = New System.Drawing.Point(424, 363)
        Me.lblIngredientesSeleccionados.MinimumSize = New System.Drawing.Size(150, 50)
        Me.lblIngredientesSeleccionados.Name = "lblIngredientesSeleccionados"
        Me.lblIngredientesSeleccionados.Size = New System.Drawing.Size(329, 50)
        Me.lblIngredientesSeleccionados.TabIndex = 7
        Me.lblIngredientesSeleccionados.Text = "Ingredientes seleccionados"
        Me.lblIngredientesSeleccionados.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblIngredientesSeleccionados.Visible = False
        '
        'btnListo
        '
        Me.btnListo.Enabled = False
        Me.btnListo.Font = New System.Drawing.Font("Berlin Sans FB", 28.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnListo.ForeColor = System.Drawing.Color.Peru
        Me.btnListo.Location = New System.Drawing.Point(833, 533)
        Me.btnListo.Name = "btnListo"
        Me.btnListo.Size = New System.Drawing.Size(230, 163)
        Me.btnListo.TabIndex = 10
        Me.btnListo.Text = "¡Listo!"
        Me.btnListo.UseVisualStyleBackColor = True
        Me.btnListo.Visible = False
        '
        'btnSeleccionarIngrediente
        '
        Me.btnSeleccionarIngrediente.BackgroundImage = Global.Formularios.My.Resources.Resources.flecha_izquierda
        Me.btnSeleccionarIngrediente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSeleccionarIngrediente.Enabled = False
        Me.btnSeleccionarIngrediente.Location = New System.Drawing.Point(305, 436)
        Me.btnSeleccionarIngrediente.Name = "btnSeleccionarIngrediente"
        Me.btnSeleccionarIngrediente.Size = New System.Drawing.Size(116, 94)
        Me.btnSeleccionarIngrediente.TabIndex = 8
        Me.btnSeleccionarIngrediente.UseVisualStyleBackColor = True
        Me.btnSeleccionarIngrediente.Visible = False
        '
        'btnDesseleccionarIngrediente
        '
        Me.btnDesseleccionarIngrediente.BackgroundImage = Global.Formularios.My.Resources.Resources.flecha_derecha
        Me.btnDesseleccionarIngrediente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDesseleccionarIngrediente.Enabled = False
        Me.btnDesseleccionarIngrediente.Location = New System.Drawing.Point(305, 602)
        Me.btnDesseleccionarIngrediente.Name = "btnDesseleccionarIngrediente"
        Me.btnDesseleccionarIngrediente.Size = New System.Drawing.Size(116, 94)
        Me.btnDesseleccionarIngrediente.TabIndex = 9
        Me.btnDesseleccionarIngrediente.UseVisualStyleBackColor = True
        Me.btnDesseleccionarIngrediente.Visible = False
        '
        'pctboxIngrediente
        '
        Me.pctboxIngrediente.BackColor = System.Drawing.Color.Transparent
        Me.pctboxIngrediente.BackgroundImage = Global.Formularios.My.Resources.Resources.ingrediente
        Me.pctboxIngrediente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pctboxIngrediente.Image = Global.Formularios.My.Resources.Resources.ingrediente
        Me.pctboxIngrediente.InitialImage = Global.Formularios.My.Resources.Resources.ingrediente
        Me.pctboxIngrediente.Location = New System.Drawing.Point(837, 138)
        Me.pctboxIngrediente.Name = "pctboxIngrediente"
        Me.pctboxIngrediente.Size = New System.Drawing.Size(150, 129)
        Me.pctboxIngrediente.TabIndex = 9
        Me.pctboxIngrediente.TabStop = False
        '
        'pctboxComida
        '
        Me.pctboxComida.BackColor = System.Drawing.Color.Transparent
        Me.pctboxComida.BackgroundImage = Global.Formularios.My.Resources.Resources.plato
        Me.pctboxComida.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pctboxComida.InitialImage = Global.Formularios.My.Resources.Resources.plato
        Me.pctboxComida.Location = New System.Drawing.Point(295, 129)
        Me.pctboxComida.Name = "pctboxComida"
        Me.pctboxComida.Size = New System.Drawing.Size(150, 129)
        Me.pctboxComida.TabIndex = 8
        Me.pctboxComida.TabStop = False
        '
        'btnVolver
        '
        Me.btnVolver.BackColor = System.Drawing.Color.Transparent
        Me.btnVolver.BackgroundImage = Global.Formularios.My.Resources.Resources.flecha_atras
        Me.btnVolver.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnVolver.Location = New System.Drawing.Point(12, 8)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(119, 107)
        Me.btnVolver.TabIndex = 11
        Me.btnVolver.UseVisualStyleBackColor = False
        '
        'frmAñadirComidaOIngrediente
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.BackgroundImage = Global.Formularios.My.Resources.Resources.chalkboard_or_blackboard
        Me.ClientSize = New System.Drawing.Size(1282, 753)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.lstDisponibles)
        Me.Controls.Add(Me.btnListo)
        Me.Controls.Add(Me.lstSeleccionados)
        Me.Controls.Add(Me.pctboxIngrediente)
        Me.Controls.Add(Me.lblIngredientesSeleccionados)
        Me.Controls.Add(Me.lblIngredientesDisponibles)
        Me.Controls.Add(Me.pctboxComida)
        Me.Controls.Add(Me.btnSeleccionarIngrediente)
        Me.Controls.Add(Me.lblQueAñadir)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.btnCambiarAIngrediente)
        Me.Controls.Add(Me.txtTipo)
        Me.Controls.Add(Me.btnCambiarAComida)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.btnDesseleccionarIngrediente)
        Me.Font = New System.Drawing.Font("Berlin Sans FB", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmAñadirComidaOIngrediente"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "¿Que hay para comer?"
        CType(Me.pctboxIngrediente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctboxComida, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnCambiarAComida As Button
    Friend WithEvents btnCambiarAIngrediente As Button
    Friend WithEvents lblQueAñadir As Label
    Friend WithEvents pctboxComida As PictureBox
    Friend WithEvents pctboxIngrediente As PictureBox
    Friend WithEvents lblNombre As Label
    Friend WithEvents lblTipo As Label
    Friend WithEvents txtNombre As TextBox
    Friend WithEvents txtTipo As TextBox
    Friend WithEvents lstDisponibles As ListBox
    Friend WithEvents lblIngredientesDisponibles As Label
    Friend WithEvents lstSeleccionados As ListBox
    Friend WithEvents lblIngredientesSeleccionados As Label
    Friend WithEvents btnSeleccionarIngrediente As Button
    Friend WithEvents btnDesseleccionarIngrediente As Button
    Friend WithEvents btnListo As Button
    Friend WithEvents btnVolver As Button
End Class
