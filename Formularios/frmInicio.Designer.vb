﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmInicio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInicio))
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.btnSugerencias = New System.Windows.Forms.Button()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.btnAñadirComidaOIngrediente = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblTitulo
        '
        Me.lblTitulo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.BackColor = System.Drawing.Color.Transparent
        Me.lblTitulo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblTitulo.Font = New System.Drawing.Font("Berlin Sans FB", 72.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.Peru
        Me.lblTitulo.Location = New System.Drawing.Point(43, 63)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(1188, 132)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Text = "¿Que hay para comer?"
        '
        'btnSugerencias
        '
        Me.btnSugerencias.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.btnSugerencias.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnSugerencias.Font = New System.Drawing.Font("Berlin Sans FB Demi", 25.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSugerencias.ForeColor = System.Drawing.Color.Peru
        Me.btnSugerencias.Location = New System.Drawing.Point(142, 259)
        Me.btnSugerencias.Name = "btnSugerencias"
        Me.btnSugerencias.Size = New System.Drawing.Size(287, 175)
        Me.btnSugerencias.TabIndex = 1
        Me.btnSugerencias.Text = "Sugerencias para cocinar"
        Me.btnSugerencias.UseVisualStyleBackColor = False
        '
        'btn1
        '
        Me.btn1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.btn1.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btn1.Font = New System.Drawing.Font("Berlin Sans FB Demi", 25.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn1.ForeColor = System.Drawing.Color.Peru
        Me.btn1.Location = New System.Drawing.Point(142, 466)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(287, 175)
        Me.btn1.TabIndex = 2
        Me.btn1.Text = "Mi plan de comidas"
        Me.btn1.UseVisualStyleBackColor = False
        '
        'btnAñadirComidaOIngrediente
        '
        Me.btnAñadirComidaOIngrediente.AutoSize = True
        Me.btnAñadirComidaOIngrediente.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnAñadirComidaOIngrediente.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.btnAñadirComidaOIngrediente.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnAñadirComidaOIngrediente.Font = New System.Drawing.Font("Berlin Sans FB Demi", 25.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAñadirComidaOIngrediente.ForeColor = System.Drawing.Color.Peru
        Me.btnAñadirComidaOIngrediente.Location = New System.Drawing.Point(492, 259)
        Me.btnAñadirComidaOIngrediente.Name = "btnAñadirComidaOIngrediente"
        Me.btnAñadirComidaOIngrediente.Size = New System.Drawing.Size(590, 60)
        Me.btnAñadirComidaOIngrediente.TabIndex = 3
        Me.btnAñadirComidaOIngrediente.Text = "Añadir Ingrediente o Comida"
        Me.btnAñadirComidaOIngrediente.UseVisualStyleBackColor = False
        '
        'frmInicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackgroundImage = Global.Formularios.My.Resources.Resources.Fondo1
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1282, 753)
        Me.Controls.Add(Me.btnAñadirComidaOIngrediente)
        Me.Controls.Add(Me.btn1)
        Me.Controls.Add(Me.btnSugerencias)
        Me.Controls.Add(Me.lblTitulo)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmInicio"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "¿Que hay para comer?"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblTitulo As Label
    Friend WithEvents btnSugerencias As Button
    Friend WithEvents btn1 As Button
    Friend WithEvents btnAñadirComidaOIngrediente As Button
End Class
